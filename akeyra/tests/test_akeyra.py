#!/usr/bin/python3
# -*- coding: utf-8 -*-
import unittest
import pwd

from .akeyra import Akeyra, copykeys


class BasicTest(unittest.TestCase):
    """Setting users list"""
    userlist = list()
    for item in pwd.getpwall():
        userlist.append(item.pw_name)

    # def test_checkurl(self):
    #     config = {
    #         'host': 'https://sakman.herokuapp.com/api/',
    #         'env': 'rec',
    #         'key': 'L2e4BTCTWQ5TDjYKuB8dzcyjpmCTSlgb',
    #         'proxy': ''}
    #     status = Akeyra.checkurl(self, config)
    #     assert status is True

    def test_checkuser(self):
        user = Akeyra.checkusers(self, 'root')
        assert user is 'root'


if __name__ == '__main__':
    unittest.main()
