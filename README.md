# akeyra :cherry_blossom:
Agent for [Sakeyra](https://github.com/LaMethode/sakeyra)

[![Python version](https://img.shields.io/pypi/pyversions/akeyra.svg)](https://img.shields.io/pypi/pyversions/akeyra.svg)
[![PyPI version](https://img.shields.io/pypi/v/akeyra.svg)](https://img.shields.io/pypi/v/akeyra.svg)
[![pipeline status](https://gitlab.com/LaMethode/akeyra/badges/master/pipeline.svg)](https://gitlab.com/LaMethode/akeyra/commits/master)
[![coverage report](https://gitlab.com/LaMethode/akeyra/badges/master/coverage.svg)](https://gitlab.com/LaMethode/akeyra/commits/master)
[![JWT](http://jwt.io/img/badge-compatible.svg)](https://jwt.io/)

## What is it?
Akeyra is the client-side of Sakeyra.  
It serves the purpose of creating/updating `~/.ssh/authorized_keys`  
It also create users that don't exist on your server but that are in the key-bundle.

## How to install ?
Use Pip `pip install akeyra`

## How to use it?
You have to fill the configuration file (see below) to connect to your SAKman Server.  
Then you just have to run `akeyra` as root.  
Make sure you have a Cron somewhere to update as frequently as possible.

## Options
usage: akeyra [-h] [-H HOST] [-E ENV] [-K KEY] [-P PROXY] [-F FILE] [-D]

You can provide all informations in CLI, use the basic configfile
(/etc/akeyra.cfg), or an alternative one. If nothing is passed by CLI, then
the basic configfile will be used.

CLI > CLI-File > base file

optional arguments:
* -h, --help            show this help message and exit
* -H HOST, --host HOST    Key Server
* -E ENV, --env ENV       Environment
* -K KEY, --key KEY       Secret key
* -P PROXY, --proxy PROXY Proxy
* -F FILE, --cnf FILE     Alt Conffile
* -D, --dry               Dry run

If you need to use a proxy, you either set environment variable like
http_proxy or use proxy in the configfile.


### Configuration file `/etc/akeyra.cfg`
```
[agent]
host =
key =
environment =
proxy =
```

### Format between Akeyra and Sakeyra (decode)
```json
{
  "environment": "rec",
  "matchlist": [
    {"local": "localuser1", "pubkey": "cleuserremote1", "email": "userremote1@test.com"},
    {"local": "localuser1", "pubkey": "cleuserremote2", "email": "userremote2@test.com"},
    {"local": "localuser1", "pubkey": "cleuserremote3", "email": "userremote3@test.com"},
    {"local": "localuser2", "pubkey": "cleuserremote1", "email": "userremote1@test.com"},
    {"local": "localuser2", "pubkey": "cleuserremote2", "email": "userremote2@test.com"},
    {"local": "localuser2", "pubkey": "cleuserremote3", "email": "userremote3@test.com"}
  ],
  "pub_date": "2017-10-18T17:15:46.799689"
  }
```
